#include "cab-utils.h"
#include "parson.h"

const char *GetFormatString(char *String, struct url_fmt *UrlFormat,
                            int ArraySize) {
  for (int i = 0; i < ArraySize; ++i) {
    if (strcmp(String, UrlFormat[i].Name) == 0) {
      return UrlFormat[i].UrlFmt;
    }
  }

  return 0;
} // GetFormatString

int GetProtocol(char *Protocol, char *Url) {
  char *Http = "http://";
  char *Https = "https://";

  char *Temp = strstr(Url, Http);
  if (Temp) {
    strcpy(Protocol, Http);
    memmove(Url, Url + strlen(Http), strlen(Url));
  }

  Temp = strstr(Url, Https);
  if (Temp) {
    strcpy(Protocol, Https);
    memmove(Url, Url + strlen(Https), strlen(Url));
  }

  return 0;
} // GetProtocol

int GetHostName(char *Hostname, char *Url) {

  char *Temp = Url;
  if (Temp) {
    int Count = 0;
    while (Temp[0] && (Temp[0] != '/')) {
      Temp++;
      Count++;
    }
    strncpy(Hostname, Url, Count);
    memmove(Url, Url + Count, strlen(Url));

    return 0;
  } // If we matched something strip everthing after '/'

  return 1;
} // GetHostName

int GetNamespace(char *Namespace, char *Url) {
  if (Url[0] == '/')
    Url++;

  int SlashIndex = 0;
  char *Slash = strstr(Url, "/");
  if (Slash) {
    // Slash will have a higher memory address, right?
    SlashIndex = Slash - Url;
  }

  if (SlashIndex > 0) {
    strncpy(Namespace, Url, SlashIndex);
    memmove(Url, Url + SlashIndex + 1, strlen(Url));
  } else {
    strncpy(Namespace, Url, strlen(Url));
    memmove(Url, Url + 1, strlen(Url));
  }

  return 0;
} // GetNamespace

void StringToLowerCase(char *String) {
  for (int i = 0; String[i]; ++i) {
    String[i] = tolower(String[i]);
  }
} // StringToLowerCase

void CreateHeader(struct curl_slist **Header, char *PrivateToken) {
  char *PrivateTokenFormat = "private-token: %s";
  char HeaderPrivateToken[100];
  sprintf(HeaderPrivateToken, PrivateTokenFormat, PrivateToken);
  *Header = curl_slist_append(*Header, "content-type: application/json");
  *Header = curl_slist_append(*Header, HeaderPrivateToken);
} // CreateHeader

size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb,
                           void *userp) {
  size_t RealSize = size * nmemb;
  struct memory_struct *Memory = (struct memory_struct *)userp;

  Memory->memory = realloc(Memory->memory, (Memory->size + RealSize + 1));
  if (Memory->memory == 0) {
    // Out of memory
    fprintf(stderr, "Error: not enough memory (realloc returned NULL)\n");
    return 0;
  }

  memcpy(&(Memory->memory[Memory->size]), contents, RealSize);
  Memory->size += RealSize;
  Memory->memory[Memory->size] = 0;

  return RealSize;
} // WriteMemoryCallback

CURLcode GetUrl(char *Url, struct curl_slist *Header, int SkipPeerVerification,
                int SkipHostnameVerification, struct memory_struct *Memory,
                struct global_flags *Flags) {
  CURL *Curl;
  CURLcode Result;

  curl_global_init(CURL_GLOBAL_ALL);

  Curl = curl_easy_init();

  if (Curl) {
    curl_easy_setopt(Curl, CURLOPT_URL, Url);

    curl_easy_setopt(Curl, CURLOPT_HTTPHEADER, Header);
    if (Flags->Verbose)
      curl_easy_setopt(Curl, CURLOPT_VERBOSE, 1L);

    curl_easy_setopt(Curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
    curl_easy_setopt(Curl, CURLOPT_WRITEDATA, (void *)Memory);

    curl_easy_setopt(Curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");

    if (SkipPeerVerification)
      curl_easy_setopt(Curl, CURLOPT_SSL_VERIFYPEER, 0L);

    if (SkipHostnameVerification)
      curl_easy_setopt(Curl, CURLOPT_SSL_VERIFYHOST, 0L);

    Result = curl_easy_perform(Curl);

    if (Result != CURLE_OK) {
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(Result));
    }

    curl_easy_cleanup(Curl);
  }

  return Result;
} // GetUrl

int PostUrl(char *Url, struct curl_slist *Header, char *Data,
                 int SkipPeerVerification, int SkipHostnameVerification,
                 struct memory_struct *Memory, struct global_flags *Flags) {
  CURL *Curl;
  int Result;
  CURLcode CurlResult;

  curl_global_init(CURL_GLOBAL_ALL);

  Curl = curl_easy_init();

  if (Curl) {
    curl_easy_setopt(Curl, CURLOPT_URL, Url);

    curl_easy_setopt(Curl, CURLOPT_HTTPHEADER, Header);
    if (Flags->Verbose)
      curl_easy_setopt(Curl, CURLOPT_VERBOSE, 1L);

    curl_easy_setopt(Curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
    curl_easy_setopt(Curl, CURLOPT_WRITEDATA, (void *)Memory);

    curl_easy_setopt(Curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");

    curl_easy_setopt(Curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_easy_setopt(Curl, CURLOPT_POSTFIELDS, Data);

    if (SkipPeerVerification)
      curl_easy_setopt(Curl, CURLOPT_SSL_VERIFYPEER, 0L);

    if (SkipHostnameVerification)
      curl_easy_setopt(Curl, CURLOPT_SSL_VERIFYHOST, 0L);

    CurlResult = curl_easy_perform(Curl);

    if (CurlResult != CURLE_OK) {
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(Result));
    }

    curl_easy_cleanup(Curl);

    curl_slist_free_all(Header);
  }

  curl_easy_getinfo (Curl, CURLINFO_RESPONSE_CODE, &Result);
  return Result;
} // PostUrl

int PutUrl(char * Url, struct curl_slist *Header, char *Data,
	   int SkipPeerVerification, int SkipHostnameVerification,
	   struct memory_struct *Memory, struct global_flags *Flags) {

  CURL *Curl;
  int Result;
  CURLcode CurlResult;
  curl_global_init(CURL_GLOBAL_ALL);

  Curl = curl_easy_init();

  if (Curl) {
    curl_easy_setopt(Curl, CURLOPT_URL, Url);

    curl_easy_setopt(Curl, CURLOPT_HTTPHEADER, Header);
    if (Flags->Verbose)
      curl_easy_setopt(Curl, CURLOPT_VERBOSE, 1L);

    curl_easy_setopt(Curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
    curl_easy_setopt(Curl, CURLOPT_WRITEDATA, (void *)Memory);

    curl_easy_setopt(Curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");

    curl_easy_setopt(Curl, CURLOPT_CUSTOMREQUEST, "PUT");

    if (SkipPeerVerification)
      curl_easy_setopt(Curl, CURLOPT_SSL_VERIFYPEER, 0L);

    if (SkipHostnameVerification)
      curl_easy_setopt(Curl, CURLOPT_SSL_VERIFYHOST, 0L);

    CurlResult = curl_easy_perform(Curl);

    if (CurlResult != CURLE_OK) {
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(Result));
      printf("Curl error code: %d", CurlResult);
    }

    curl_easy_cleanup(Curl);

    curl_slist_free_all(Header);
  }

  curl_easy_getinfo(Curl, CURLINFO_RESPONSE_CODE, &Result);
  return Result;
} // PutUrl

int JsonToHttpParameter(char *Data) {
  JSON_Value *RawValue = json_parse_string(Data);
  JSON_Object *Object = json_value_get_object(RawValue);
  char NewData[5000] = "?";
  for (int i = 0; i < json_object_get_count(Object); ++i) {
    if (i > 0)
      strcat(NewData, "&");
    char *Field = (char *)json_object_get_name(Object, i);
    strcat(NewData, Field);
    strcat(NewData, "=\"");
    strcat(NewData, json_object_get_string(Object, Field));
      strcat(NewData, "\"");
  }

  strcpy(Data, NewData);
  return 0;
}

int RunCommand(struct project_data *ProjectData, struct options *Options,
               struct global_flags *Flags, struct command_struct Commands[],
               int CommandsIndex) {

  int FoundCommand = 0;

  for (int i = 0; i < CommandsIndex; ++i) {
    if (Options->ArgumentVector) {
      if (strcmp(Options->ArgumentVector[0], Commands[i].Command) == 0) {
        Commands[i].Function(ProjectData, Options, Flags);
        FoundCommand = 1;
        break;
      }
    }
  }

  // Fail if we didn't find a command
  if (FoundCommand <= 0)
    return 1;

  return 0;
} // RunCommand

