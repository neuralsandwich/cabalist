#include "cab.h"
#include "commands.h"

const char cab_usage_string[] = "cab [--help]\n";

global_variable struct command_struct Commands[] = {
  { "help", HelpCommand }, { "merge", MergeCommand },
  { "project", ProjectCommand }
};

internal void HandleOptions(struct options *Options,
                            struct global_flags *Flags) {

  // Decrement argc and increment argv as the commands name is the first
  // arguement.
  // e.g. "cab --help", argc == 2, argv == { "cab", "--help" }
  Options->ArgumentCount--;
  Options->ArgumentVector++;
  if (Options->ArgumentCount) {
    while (Options->ArgumentCount > 0) {
      char *Argument = *Options->ArgumentVector;
      // If the argument isn't a flag we are done here.
      // All other arguments _should_ be commands and their flags
      if (Argument[0] != '-')
        break;

      switch (Argument[1]) {

      case 'v': {
        Flags->Verbose = VERBOSE;
      } break;

      case 'h': {
        Flags->NeedsHelp = TRUE;
      } break;

      default: { break; }
      }

      // Decrement argc as we have dealt with options
      Options->ArgumentCount--;
      // Increment argv as we need to deal with the next option
      Options->ArgumentVector++;
    }
  } else {
    Flags->NeedsHelp = TRUE;
  }
} // HandleOptions

internal int LoadProjectConfig(struct project_data *ProjectData) {
  // pathconf retrieve the max path size for the file
  char *Cabconfig = ".cabconfig";

  // Parse .cabalistrc
  JSON_Value *RootValue = json_parse_file(Cabconfig);
  if (json_value_get_type(RootValue) != JSONObject) {
    printf("LoadProjectConfig(): Could no lot JSONObject\n");
  }

  JSON_Object *ConfigObject = json_value_get_object(RootValue);
  strcpy(ProjectData->Service, json_object_get_string(ConfigObject, "host"));

  char Url[256];
  if (json_object_get_string(ConfigObject, "url")) {
    strcpy(Url, json_object_get_string(ConfigObject, "url"));
    GetProtocol(ProjectData->Protocol, Url);
    GetHostName(ProjectData->Hostname, Url);
    GetNamespace(ProjectData->Namespace, Url);
    // This function does exactly what we need to get the Projectname
    // Maybe we should rename it?
    // TODO: Track down Seg fault here somewhere
    GetNamespace(ProjectData->ProjectName, Url);

    return 0;
  } else {
    // TODO: offer user some feed back about the config file.
    fprintf(stderr, "LoadProjectConfig(): Config is not correct\n");
  }

  return 1;
} // LoadProjectConfig

internal int LoadUserTokens(struct project_data *ProjectData) {
  // pathconf retrieve the max path size for the file
  char *Home = getenv("HOME");
  char *CabalistRcPath =
      (char *)calloc(pathconf(Home, _PC_NAME_MAX), sizeof(char));
  sprintf(CabalistRcPath, "%s/.cabalistrc", Home);

  // Parse .cabalistrc
  JSON_Value *RootValue = json_parse_file(CabalistRcPath);
  if (json_value_get_type(RootValue) != JSONObject) {
    fprintf(stderr, "LoadUserTokens(): Could not load JSONObject\n");
  }

  JSON_Object *TokenObject = json_value_get_object(RootValue);
  for (size_t i = 0; i < json_object_get_count(TokenObject); ++i) {
    char *LoweredUrl = (char *)json_object_get_name(TokenObject, i);
    StringToLowerCase(LoweredUrl);
    char *LoweredHostname = ProjectData->Hostname;
    StringToLowerCase(LoweredHostname);
    char *thing = strstr(LoweredUrl, LoweredHostname);
    if (thing != NULL) {
      strcpy(ProjectData->PrivateToken,
             json_object_get_string(TokenObject, LoweredUrl));
      return 0;
    }
  } // For every 'Object' in the Object
  return 1;
} // LoadUserToken

int main(int argc, char *argv[], char *envp[]) {
  // TODO: We should just go ahead and malloc all of these.
  // It should make clean up easier.
  struct project_data *ProjectData =
      (struct project_data *)calloc(1, sizeof(struct project_data));
  struct global_flags *Flags =
      (struct global_flags *)calloc(1, sizeof(struct global_flags));

  struct options Options = { 0 };
  Options.ArgumentCount = argc;
  Options.ArgumentVector = argv;

  HandleOptions(&Options, Flags);

  if (LoadProjectConfig(ProjectData))
    // TODO: Error reporting
    return 1;

  if (LoadUserTokens(ProjectData))
    // TODO: Error reporting
    return 1;

  // If -h or --help was specified then ignore everything else
  // the user needs help.
  int Status;
  if (Flags->NeedsHelp) {
    for (int i = 0; i < ArrayCount(Commands); ++i) {
      if (strcmp("help", Commands[i].Command) == 0) {
        Commands[i].Function(ProjectData, &Options, Flags);
      }
    }
  } else {
    Status = RunCommand(ProjectData, &Options, Flags, Commands,
                        ArrayCount(Commands));
    free((void *)ProjectData);
    free((void *)Flags);
  }

  curl_global_cleanup();

  return Status;
} // main
