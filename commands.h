#ifndef COMMANDS_H
#define COMMANDS_H
#include <stdlib.h>
#include "cab-structs.h"
int MergeCommand(struct project_data *ProjectData, struct options *Options,
                struct global_flags *flags);
int HelpCommand(struct project_data *ProjectData, struct options *Options,
                struct global_flags *flags);
int ProjectCommand(struct project_data *ProjectData, struct options *Options,
                   struct global_flags *flags);
#endif
