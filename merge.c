#include "commands.h"
#include "cab-defines.h"
#include "cab-utils.h"
#include "parson.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

// Key pair struct
// Stores all of the format urls for GitLab merge requests
// TODO: Do we need the uninserted character count?
global_variable struct url_fmt MergeUrlFormats[] = {
    {"get merge requests opened",
     "%s%s/api/v3/projects/%s%%2F%s/merge_requests?state=opened", 44},
    {"get merge requests closed",
     "%s%s/api/v3/projects/%s%%2F%s/merge_requests?state=closed", 44},
    {"get merge requests merged",
     "%s%s/api/v3/projects/%s%%2F%s/merge_requests?state=merged", 44},
    {"get merge requests all",
     "%s%s/api/v3/projects/%s%%2F%s/merge_requests?state=all", 42},
    {"get merge request", "%s%s/api/v3/projects/%s%%2F%s/merge_request/%s", 32},
    {"post merge request create",
     "%s%s/api/v3/projects/%s%%2F%s/merge_requests", 32},
    {"put merge request close",
     "%s%s/api/v3/projects/%s%%2F%s/merge_request/%s?state_event=close", 51},
    {"put merge request reopen",
     "%s%s/api/v3/projects/%s%%2F%s/merge_request/%s?state_event=reopen", 52},
    {"put merge request accept",
     "%s%s/api/v3/projects/%s%%2F%s/merge_request/%s/merge", 38}};

// TODO: Figure out if this is the best way to store this
char *CreateMerge = "{ \"title\": \"%s\", \"source_branch\": \"%s\", "
                    "\"target_branch\": \"%s\" }";

internal int MergeHelp(struct project_data *ProjectData,
                       struct options *Options, struct global_flags *Flags) {

  char *MergeHelpString =
      "usage:\n     cab merge help\n"
      "  or cab merge list [all] [closed] [merged] [opened]\n"
      "  or cab merge show <id>\n"
      "  or cab merge create (--title|-t <title>)"
      " (--source|-s <source branch>)"
      " (--target|-b <target branch>)\n"
      "  or cab merge close <id>\n"
      "  or cab merge reopen <id>\n"
      "  or cab merge accept <id>\n";

  printf("%s\n", MergeHelpString);
  return 0;
} // MergeHelp

internal int MergeShow(struct project_data *ProjectData,
                       struct options *Options, struct global_flags *Flags) {

  // MergeShow need merge request id
  if (Options->ArgumentCount == 0)
    return -1;

  struct curl_slist *Header = 0;
  struct memory_struct Memory = {0};

  Memory.memory = malloc(1);
  Memory.size = 0;

  CreateHeader(&Header, ProjectData->PrivateToken);

  char Url[2048];
  sprintf(Url,
          GetFormatString("get merge request", MergeUrlFormats,
                          (sizeof(MergeUrlFormats) / sizeof(struct url_fmt))),
          ProjectData->Protocol, ProjectData->Hostname, ProjectData->Namespace,
          ProjectData->ProjectName, Options->ArgumentVector[1]);
  CURLcode Result =
      GetUrl(Url, Header, PEER_VERIFICATION, HOST_VERIFICATION, &Memory, Flags);

  int Success = FALSE;
  if (Result == CURLE_OK) {
    JSON_Value *RootValue;

    RootValue = json_parse_string(Memory.memory);
    if (json_value_get_type(RootValue) != JSONObject) {
      fprintf(stderr, "MergeGetRequest(): Reply is not JSONObject\n");
      return 0;
    }

    JSON_Object *ReplyObject = json_value_get_object(RootValue);
    fprintf(stdout,
            "Merge Request: %1.f\nTitle: %s\nFrom: %s To: %s\nState: %s\n",
            json_object_get_number(ReplyObject, "id"),
            json_object_dotget_string(ReplyObject, "title"),
            json_object_dotget_string(ReplyObject, "source_branch"),
            json_object_dotget_string(ReplyObject, "target_branch"),
            json_object_dotget_string(ReplyObject, "state"));
    fprintf(stdout, "Description:\n\n%s\n",
            json_object_dotget_string(ReplyObject, "description"));
    Success = TRUE;
  } // CURLE_OK

  if (Memory.memory) {
    free(Memory.memory);
  }

  if (Success)
    return 0;

  return 1;

} // MergeGetRequest

internal int MergeList(struct project_data *ProjectData,
                       struct options *Options, struct global_flags *Flags) {
  struct curl_slist *Header = 0;
  struct memory_struct Memory = {0};

  Memory.memory = malloc(1);
  Memory.size = 0;

  CreateHeader(&Header, ProjectData->PrivateToken);

  char Url[2048];
  Options->ArgumentCount--;
  Options->ArgumentVector++;

  // Pick the right format string and generate the correct Url
  if (Options->ArgumentCount == 0) {
    sprintf(Url,
            GetFormatString("get merge requests opened", MergeUrlFormats,
                            (sizeof(MergeUrlFormats) / sizeof(struct url_fmt))),
            ProjectData->Protocol, ProjectData->Hostname,
            ProjectData->Namespace, ProjectData->ProjectName);
  } else if (strcmp(Options->ArgumentVector[0], "closed") == 0) {
    sprintf(Url,
            GetFormatString("get merge requests closed", MergeUrlFormats,
                            (sizeof(MergeUrlFormats) / sizeof(struct url_fmt))),
            ProjectData->Protocol, ProjectData->Hostname,
            ProjectData->Namespace, ProjectData->ProjectName);
  } else if (strcmp(Options->ArgumentVector[0], "opened") == 0) {
    sprintf(Url,
            GetFormatString("get merge requests opened", MergeUrlFormats,
                            (sizeof(MergeUrlFormats) / sizeof(struct url_fmt))),
            ProjectData->Protocol, ProjectData->Hostname,
            ProjectData->Namespace, ProjectData->ProjectName);
  } else if (strcmp(Options->ArgumentVector[0], "merged") == 0) {
    sprintf(Url,
            GetFormatString("get merge requests merged", MergeUrlFormats,
                            (sizeof(MergeUrlFormats) / sizeof(struct url_fmt))),
            ProjectData->Protocol, ProjectData->Hostname,
            ProjectData->Namespace, ProjectData->ProjectName);
  } else if (strcmp(Options->ArgumentVector[0], "all") == 0) {
    sprintf(Url,
            GetFormatString("get merge requests all", MergeUrlFormats,
                            (sizeof(MergeUrlFormats) / sizeof(struct url_fmt))),
            ProjectData->Protocol, ProjectData->Hostname,
            ProjectData->Namespace, ProjectData->ProjectName);
  }

  CURLcode Result =
      GetUrl(Url, Header, PEER_VERIFICATION, HOST_VERIFICATION, &Memory, Flags);

  int Success = FALSE;
  if (Result == CURLE_OK) {
    JSON_Value *RootValue;

    RootValue = json_parse_string(Memory.memory);
    if (json_value_get_type(RootValue) != JSONArray) {
      fprintf(stderr, "MergeList(): Reply is not JSONArray\n");
      return 0;
    }

    JSON_Array *Reply = json_value_get_array(RootValue);
    JSON_Object *ReplyObject;
    for (int i = 0; i < json_array_get_count(Reply); ++i) {
      ReplyObject = json_array_get_object(Reply, i);
      fprintf(stdout, "%1.f: %s (%s)\n",
              json_object_get_number(ReplyObject, "id"),
              json_object_dotget_string(ReplyObject, "title"),
              json_object_dotget_string(ReplyObject, "state"));
    } // Objects in array
    Success = TRUE;
  } // CURLE_OK

  if (Memory.memory) {
    free(Memory.memory);
  }

  if (Success)
    return 0;

  return 1;
} // MergeList

int MergeCreate(struct project_data *ProjectData, struct options *Options,
                struct global_flags *Flags) {
  struct curl_slist *Header = 0;
  struct memory_struct Memory = {0};

  Memory.memory = malloc(1);
  Memory.size = 0;

  CreateHeader(&Header, ProjectData->PrivateToken);

  char Url[2048];
  char Data[5000];
  char Title[256] = {0};
  char SourceBranch[256] = {0};
  char TargetBranch[256] = {0};
  Options->ArgumentCount--;
  Options->ArgumentVector++;

  if (Options->ArgumentCount == 0) {
    fprintf(stderr, "MergeCreate(): Requires arugments");
    return 1;
  }

  // Parse --title, --source, and --target
  while (Options->ArgumentCount) {
    // --title <title> or -t <title>
    if (strcmp(Options->ArgumentVector[0], "-") == 0) {
      if ((strcmp(Options->ArgumentVector[0], "--title") == 0) ||
          (strcmp(Options->ArgumentVector[0], "-t") == 0)) {
        Options->ArgumentCount--;
        Options->ArgumentVector++;
        if (Options->ArgumentCount) {
          strcpy(Title, Options->ArgumentVector[0]);
          Options->ArgumentCount--;
          Options->ArgumentVector++;
        }
      }
      // --source <source> | -s <source>
      else if ((strcmp(Options->ArgumentVector[0], "--source") == 0) ||
               (strcmp(Options->ArgumentVector[0], "-s") == 0)) {
        Options->ArgumentCount--;
        Options->ArgumentVector++;
        if (Options->ArgumentCount) {
          strcpy(SourceBranch, Options->ArgumentVector[0]);
          Options->ArgumentCount--;
          Options->ArgumentVector++;
        }
      }
      // --target <targe> | -b <targer>
      // NOTE: -b is used here to mean base branch, while source is technically
      //       the head branch... a bit confusing but it stops the -t conflict
      else if ((strcmp(Options->ArgumentVector[0], "--target") == 0) ||
               (strcmp(Options->ArgumentVector[0], "-b") == 0)) {
        Options->ArgumentCount--;
        Options->ArgumentVector++;
        if (Options->ArgumentCount) {
          strcpy(TargetBranch, Options->ArgumentVector[0]);
          Options->ArgumentCount--;
          Options->ArgumentVector++;
        }
      } else {
        Options->ArgumentCount--;
        Options->ArgumentVector++;
      }
    } else {
      Options->ArgumentCount--;
      Options->ArgumentVector++;
    } // Find --title, --source, and --target
  }   // While there are arguments

  int Success = FALSE;
  if ( (Title[0] != 0) && (SourceBranch != 0) && (TargetBranch != 0)) {
    sprintf(Url,
            GetFormatString("post merge request create", MergeUrlFormats,
                            (sizeof(MergeUrlFormats) / sizeof(struct url_fmt))),
            ProjectData->Protocol, ProjectData->Hostname,
            ProjectData->Namespace, ProjectData->ProjectName);
    sprintf(Data, CreateMerge, Title, SourceBranch, TargetBranch);

    int Result = PostUrl(Url, Header, Data, PEER_VERIFICATION,
                         HOST_VERIFICATION, &Memory, Flags);
    switch (Result) {
    case 201: {
      JSON_Value *RootValue;
      RootValue = json_parse_string(Memory.memory);
      if (json_value_get_type(RootValue) != JSONObject) {
        fprintf(stderr, "MergeCreate(): Reply is not JSONObject\n");
        Success = 0;
        break;
      }

      JSON_Object *ReplyObject = json_value_get_object(RootValue);
      fprintf(stdout, "%1.f: %s (%s)\n",
              json_object_get_number(ReplyObject, "id"),
              json_object_dotget_string(ReplyObject, "title"),
              json_object_dotget_string(ReplyObject, "state"));
      Success = TRUE;
    } break;
    case 409: {
      fprintf(stderr, "Merge request already exists\n");
      Success = FALSE;
    } break;
    case 400: {
      fprintf(stderr, "Bad requests to %s\n", Url);
    } break;
    }
  } else {
    // TODO: Error handling
    puts("cab merge create: Needs title, source and target branch");
    Success = FALSE;
  }

  if (Memory.memory) {
    free(Memory.memory);
  }

  if (Success != FALSE)
    return 0;

  return 1;
} // MergeCreate

int MergeClose(struct project_data *ProjectData, struct options *Options,
               struct global_flags *Flags) {
  struct curl_slist *Header = 0;
  struct memory_struct Memory = {0};

  Memory.memory = malloc(1);
  Memory.size = 0;

  CreateHeader(&Header, ProjectData->PrivateToken);

  char Url[2048];
  char Data[5000];

  // TODO: Deal with parameters
  Options->ArgumentCount--;
  Options->ArgumentVector++;

  int Success = FALSE;
  sprintf(Url,
          GetFormatString("put merge request close", MergeUrlFormats,
                          (sizeof(MergeUrlFormats) / sizeof(struct url_fmt))),
          ProjectData->Protocol, ProjectData->Hostname, ProjectData->Namespace,
          ProjectData->ProjectName, Options->ArgumentVector[0]);

  int Result = PutUrl(Url, Header, Data, PEER_VERIFICATION, HOST_VERIFICATION,
                      &Memory, Flags);

  return 0;
} // MergeClose

int MergeReopen(struct project_data *ProjectData, struct options *Options,
                struct global_flags *Flags) {
  struct curl_slist *Header = 0;
  struct memory_struct Memory = {0};

  Memory.memory = malloc(1);
  Memory.size = 0;

  CreateHeader(&Header, ProjectData->PrivateToken);

  char Url[2048];
  char Data[5000];

  // TODO: Deal with parameters
  Options->ArgumentCount--;
  Options->ArgumentVector++;

  int Success = FALSE;
  sprintf(Url,
          GetFormatString("put merge request reopen", MergeUrlFormats,
                          (sizeof(MergeUrlFormats) / sizeof(struct url_fmt))),
          ProjectData->Protocol, ProjectData->Hostname, ProjectData->Namespace,
          ProjectData->ProjectName, Options->ArgumentVector[0]);

  int Result = PutUrl(Url, Header, Data, PEER_VERIFICATION, HOST_VERIFICATION,
                      &Memory, Flags);

  return 0;
} // MergeReopen

int MergeAccept(struct project_data *ProjectData, struct options *Options,
                struct global_flags *Flags) {
  struct curl_slist *Header = 0;
  struct memory_struct Memory = {0};

  Memory.memory = malloc(1);
  Memory.size = 0;

  CreateHeader(&Header, ProjectData->PrivateToken);

  char Url[2048];
  char Data[5000];

  // TODO: Deal with parameters
  Options->ArgumentCount--;
  Options->ArgumentVector++;

  int Success = FALSE;
  sprintf(Url,
          GetFormatString("put merge request accept", MergeUrlFormats,
                          (sizeof(MergeUrlFormats) / sizeof(struct url_fmt))),
          ProjectData->Protocol, ProjectData->Hostname, ProjectData->Namespace,
          ProjectData->ProjectName, Options->ArgumentVector[0]);

  int Result = PutUrl(Url, Header, Data, PEER_VERIFICATION, HOST_VERIFICATION,
                      &Memory, Flags);

  /*
   * If merge success you get 200 OK.
   *
   * If it has some conflicts and can not be merged - you get 405 and error
   * message 'Branch cannot be merged'
   *
   * If merge request is already merged or closed - you get 405 and error
   *message
   * 'Method Not Allowed'
   *
   * If you don't have permissions to accept this merge request - you'll get a
   *401
   */

  return 0;
} // MergeAccept

// Need to keep all the functions here for RunCommand
global_variable struct command_struct Commands[] = {{"help", MergeHelp},
                                                    {"list", MergeList},
                                                    {"create", MergeCreate},
                                                    {"close", MergeClose},
                                                    {"accept", MergeAccept},
                                                    {"reopen", MergeReopen},
                                                    {"show", MergeShow}};

int MergeCommand(struct project_data *ProjectData, struct options *Options,
                 struct global_flags *Flags) {
  Options->ArgumentCount--;
  Options->ArgumentVector++;
  int Status;
  if (Flags->NeedsHelp) {
    for (int i = 0; i < ArrayCount(Commands); ++i) {
      if (strcmp("help", Commands[i].Command) == 0) {
        Status = Commands[i].Function(ProjectData, Options, Flags);
      }
    }
  } else {
    Status =
        RunCommand(ProjectData, Options, Flags, Commands, ArrayCount(Commands));
  }

  if (Status)
    return 1;

  return 0;
} // MergeCommand
