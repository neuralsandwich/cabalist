#ifndef CAB_DEFINES_H
#define CAB_DEFINES_H
#define SKIP_PEER_VERIFICATION 1
#define SKIP_HOST_VERIFICATION 1
#define PEER_VERIFICATION 0
#define HOST_VERIFICATION 0
#define QUIET 0
#define VERBOSE 1
#define TRUE 1
#define FALSE 0
#define Assert(Expression)                                                     \
  if (!(Expression)) {                                                         \
    *(int *)0 = 0;                                                             \
  }

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

#define global_variable static
#define internal static
#define local_persist static

#endif
