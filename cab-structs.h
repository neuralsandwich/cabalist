#ifndef CAB_STRUCTS_H
#define CAB_STRUCTS_H

struct url_fmt {
  const char *Name;
  const char *UrlFmt;
  const int Length;
};

struct memory_struct {
  char *memory;
  size_t size;
};

struct project_data {
  // Project information
  // Plenty of space for 'http://', 'https://'?
  char Protocol[10];
  // Apparently the maximum hostname is 253 characters
  char Hostname[256];
  char Namespace[256];
  char ProjectName[256];
  // GitLab, GitHub, BitBucket, these are all under
  char Service[20];

  // User Token
  // Large enough?
  char PrivateToken[100];
};

struct global_flags {
  int Verbose;
  int NeedsHelp;
};

struct options {
  int ArgumentCount;
  char **ArgumentVector;
};

struct command_struct {
  const char *Command;
  int (*Function)(struct project_data *ProjectData, struct options *Options,
                  struct global_flags *flags);
};
#endif
