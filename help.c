#include "commands.h"
#include <stdio.h>

int HelpCommand(struct project_data *ProjectData, struct options *Options,
                struct global_flags *flags) {

  char *HelpString = "Usage:\n    cab help\n"
    "    cab merge (list|show|create|close|reopen|accept)\n";
  printf("%s\n", HelpString);

  return 0;
}
