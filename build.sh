#!/bin/bash -xe

COMMANDSSRC="merge.c help.c project.c cab-utils.c"
${1} -ansi -g3 -O0 -m64 -c ${COMMANDSSRC}
ar rcs libcab.a *.o
${1} -ansi -g3 -O0 -m64 -DDEBUG=1 -o cab -lcurl cab.c parson.c libcab.a
