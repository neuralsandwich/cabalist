#ifndef CAB_UTILS_H
#define CAB_UTILS_H
#include <curl/curl.h>
#include <stdlib.h>
#include <string.h>
#include "cab-structs.h"
#include "cab-defines.h"

// Functions
const char *GetFormatString(char *String, struct url_fmt *UrlFormat,
                            int ArraySize);
int GetProtocol(char *Protocol, char *Url);
int GetHostName(char *Hostname, char *Url);
int GetNamespace(char *Namespace, char *Url);
void StringToLowerCase(char *String);
void CreateHeader(struct curl_slist **Header, char *PrivateToken);
size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb,
                           void *userp);
CURLcode GetUrl(char *Url, struct curl_slist *Header, int SkipPeerVerification,
                int SkipHostnameVerification, struct memory_struct *Memory,
                struct global_flags *Flags);
int PostUrl(char *Url, struct curl_slist *Header, char *Data,
            int SkipPeerVerification, int SkipHostnameVerification,
            struct memory_struct *Memory, struct global_flags *Flags);
int RunCommand(struct project_data *ProjectData, struct options *Options,
               struct global_flags *Flags, struct command_struct *Commands,
               int CommandsIndex);
#endif
