#include "commands.h"
#include "cab-defines.h"
#include "parson.h"
#include "cab-utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

/* TODO list:
 *
 * Add parameters:
 *   * archived (optional) - if passed, limit by archived status
 *   * order_by (optional) - Return requests ordered by id, name, created_at
 *                           or last_activity_at
 *   * sort (optional)     - Return requests sorted in asc or desc order
 *   * search (optional)   - Return list of authorized projects according to a
 *                           seach criteria
 *
 *   * owned  - 'GET /projects/owned'
 *   * all    - 'GET /projects/all'
 *   * single - 'GET /projects/:id' or 'GET /projects/$namespace%2F$project'
 *   * events - 'GET /projects/:id/events'
 *   * create - 'POST /projects' with:
 *     * name (required)           - new project name
 *     * path (optional)           - custom respoitory name for new project.
 *     * namespace_id (optional)   - namespace for the new project (defaults to
 *                                 user)
 *     * description (optional)    - short project description
 *     * issues_enabled (optional)
 *     * merge_requests_enabled (optional)
 *     * wiki_enabled (optional)
 *     * snippets_enabled (optional)
 *     * public (optional)         - if true same as setting visibility_level
 *                                   to 20
 *     * visibility_level (optional)
 *     * import_url (optional)
 *   * create_for_user - 'POST /projects/user/:user_id' with:
 *     * user_id (required)        - user_id of owner
 *     * name (required)           - new project name
 *     * path (optional)           - custom respoitory name for new project.
 *     * namespace_id (optional)   - namespace for the new project (defaults to
 *                                 user)
 *     * description (optional)    - short project description
 *     * issues_enabled (optional)
 *     * merge_requests_enabled (optional)
 *     * wiki_enabled (optional)
 *     * snippets_enabled (optional)
 *     * public (optional)         - if true same as setting visibility_level
 *                                   to 20
 *     * visibility_level (optional)
 *     * import_url (optional)
 *   * fork - 'POST /projects/fork/:id'
 *   * delete - 'DELETE /projects/:id'
 */

int ProjectCommand(struct project_data *ProjectData, struct options *Options,
                   struct global_flags *Flags) {

  struct curl_slist *Header = 0;
  struct memory_struct Memory = { 0 };
    // TODO: Make protocol dynamic
  struct url_fmt ProjectUrlFormats[] = {
    { "get projects", "%s%s/api/v3/projects", 24 }
  };

  Memory.memory = malloc(1);
  Memory.size = 0;

  CreateHeader(&Header, ProjectData->PrivateToken);

  char Url[100];
  sprintf(Url,
          GetFormatString("get projects", ProjectUrlFormats,
                          (sizeof(ProjectUrlFormats) / sizeof(struct url_fmt))),
                           ProjectData->Protocol, ProjectData->Hostname);
  CURLcode Result =
      GetUrl(Url, Header,
             PEER_VERIFICATION, HOST_VERIFICATION, &Memory, Flags);

  if (Result == CURLE_OK) {
    JSON_Value *RootValue;

    RootValue = json_parse_string(Memory.memory);
    if (json_value_get_type(RootValue) != JSONArray) {
      fprintf(stderr, "ProjectCommand(): Reply is not JSONArray\n");
      return 0;
    }

    JSON_Array *Reply = json_value_get_array(RootValue);
    JSON_Object *thing;
    for (int i = 0; i < json_array_get_count(Reply); ++i) {
      thing = json_array_get_object(Reply, i);
      fprintf(stdout, "%s\n",
              json_object_dotget_string(thing, "name_with_namespace"));
    } // Objects in array
  } // CURLE_OK

  if (Memory.memory) {
    free(Memory.memory);
  }

  return 1;
} // CommandProjects
