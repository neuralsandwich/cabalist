#!/bin/bash -e

# install.sh: Installs Cabalist on to local systems.
# Author: Sean Jones <neuralsandwich@gmail.com>
#
# Usage:
#     ./insatll.sh [prefix]

readonly DEFAULT_PREFIX="/usr/local/bin/"

abort()
{
  echo "ERROR: $@"
  exit 1
}

is_set()
{
  local var="$1"

  [[ -n "$var" ]]
}

is_found()
{
  [[ -f "./cab" ]]
}

install()
{
  sudo cp -v ./cab "$prefix"
}

main()
{
  local prefix="$1"
  local install_ok=false

  is_set "$prefix" || prefix="$DEFAULT_PREFIX"

  is_found cab && install_ok=true \
    || abort "Cannot find cabalist binary"

  if [[ "$install_ok" == "true" ]]; then
    install
  fi

}

main "$@"
