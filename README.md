# Cabalist

[![Build
Status](https://travis-ci.org/NeuralSandwich/cabalist.svg?branch=develop)](https://travis-ci.org/NeuralSandwich/cabalist)

The commandline tool for GitLab

# Usage

    cab help
    cab merge (list|show|create|accept|close|reopen)

## Configuration files

### ~/.cabalistrc

```json
{
    "gitlab.com": "private-token",
    "gitlab.example.com": "private-token"
}
```

### .cabconfig

```json
{
    "project": "project",
    "uri": "repository uri",
    "host": "GitLab"
}
```

### .editorconfig
This is a future feature
```
# EditorConfig is awesome: http://EditorConfig.org

# top-most EditorConfig file
root = true

# Unix-style newlines with a newline ending every file
[*]
end_of_line = lf
insert_final_newline = true

# 4 space indentation
[*.py]
indent_style = space
indent_size = 4

# Tab indentation (no size specified)
[*.js]
indent_style = tab
```
